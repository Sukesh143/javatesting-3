/**
 * 
 */
package biz.binarystar.testing.InterviewSample1;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * @author suhaib
 *
 */
public class ATMMachineTest extends TestCase {
	
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ATMMachineTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ATMMachineTest.class );
    }

    /**
     * In this case we will try to open an account using some details. Then we would
     * do some deposits and withdrawals and check the remaining balance.
     */
    public void testCase1()
    {
    	ATMMachine machine = ATMMachine.getInstance();
    	
    	String accountNumber = machine.openAccount("checking", 10000);
    	machine.debit(accountNumber, 1500);
    	machine.debit(accountNumber, 100.32);
    	machine.credit(accountNumber, 2000);
    	double finalBalance = machine.getBalance(accountNumber);
        assertEquals(9600.32, finalBalance, 0.001);
    }
    
    /**
     * In this case we will try to open an account with opening balance less than
     * $500. And will see if our account opening is successful or not.
     */
    public void testCase2()
    {
    	String accountNumber = "";
    	try {
    		ATMMachine machine = ATMMachine.getInstance();
    		accountNumber = machine.openAccount("checking", 100);    		
    		assertTrue(false);
    	} catch(Exception e) {
    		assertNotNull(accountNumber);
    		assertFalse(accountNumber.isEmpty());
    	}
        
    }
    
    /**
     * In this case we will try to open account with $10000 and then credit $9800
     * from it. We then need to verify if account is flagged or not.
     */
    public void testCase3()
    {
    	String accountNumber = "";
    	try {
    		ATMMachine machine = ATMMachine.getInstance();
    		accountNumber = machine.openAccount("checking", 10000);    		
    		machine.credit(accountNumber, 9800);
    		assertTrue(machine.isFlagged(accountNumber));
    	} catch(Exception e) {
    		assertTrue(false);
    	}
    }
    
    /**
     * BONUS: Implement the runnable interface and read transactions from transactions.txt
     * After running it make sure account is properly flagged if required.
     */
    public void testCase4() {
    	String accountNumber = "10192999";
    	Thread thread1 = new Thread(ATMMachine.getInstance());
    	thread1.start();
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	assertEquals(1076.68, ATMMachine.getInstance().getBalance(accountNumber), 0.001);
    	assertFalse(ATMMachine.getInstance().isFlagged(accountNumber));
    }

}
